from fraccion import Fraccion
class Serie:

    def __init__(self, diccionario):#resive un diccionario con los valores de las resistencias en el circuito
        self.resistencia_linea = diccionario

    def suma_serie(self): #recorre los valores del diccionario de datos y los suma teniendo en cuenta la formula del circuito
        suma = 0.0
        for i in self.resistencia_linea:
            indenti = self.identifica(i)
            if indenti:
                paralelo = Paralelo(self.resistencia_linea[i])
                rq = paralelo.suma_paralelo()
                suma = suma + rq
            else:
                valor = self.resistencia_linea[i]
                suma += valor
        return suma

    def identifica(self,key):
        if key == 'paralelo':
            return True
        else:
            return False

class Paralelo:

    def __init__(self,diccionario):#mismos requisitos que en serie
        self.resistencias = diccionario

    def suma_paralelo(self):
        aux = 1
        for i in self.resistencias:
            id = self.identificar(i)
            if id:
                serie = Serie(self.resistencias[i])
                rq = serie.suma_serie()
                fau = Fraccion(1,rq)
                if aux == 1:
                    suma = fau
                else:
                    valor = fau
            else:
                frac = Fraccion(1,self.resistencias[i])
                if aux == 1:
                    suma = frac
                else:
                    valor = frac
            if aux != 1:
                suma = suma.__add__(valor)
            aux += 1
        suma = suma.invertir_simpli()
        return suma.valor()
                
    def identificar(self,key):
        if key == 'serie':
            return True
        else:
            return False

