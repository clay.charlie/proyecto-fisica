# busca el maximo comun divisor
def mcd(m, n):
    while m % n != 0:
        mViejo = m
        nViejo = n

        m = nViejo
        n = mViejo % nViejo
    return n

class Fraccion:

    #define el objeto fraccion
    def __init__(self,arriba,abajo):
        self.num = arriba
        self.den = abajo

    #suma dos fracciones
    def __add__(self, otraFraccion):
        nuevoNum = self.num * otraFraccion.den + self.den * otraFraccion.num
        nuevoDen = self.den * otraFraccion.den
        return Fraccion(nuevoNum, nuevoDen)

    #invierte la fraccion y saca el comun denominador para simplificar
    def invertir_simpli(self):
        max_cm_d = mcd(self.num,self.den)
        self.num = self.num // max_cm_d
        self.den = self.den // max_cm_d
        return Fraccion(self.den, self.num)

    #retorna el resultado de la divicion de los valores de la fraccion
    def valor(self):
        return float(self.num/self.den)

    def imprimir(self):
        return f'{self.num}/{self.den}'
